# Flexget (http://www.flexget.com) configuration file.
#
# Created by: Jeff Wilson <jeff@jeffalwilson.com>
# Modified by: Matt Tallant <m.tallant+github@gmail.com>
#
# This config file is not a standard Flexget config, it is used
# to sort my unrared movies and TV shows into their proper place
#
# Use the secrets plugin to separately store credentials, etc
variables: secretfile.yml
templates:
  global:
    # Each feed must be run manually
    manual: yes
    # Disable seen, alway process files
    disable: [seen, remember_rejected, retry_failed] 
    accept_all: yes
    # Make sure all filenames are Windows safe (for samba)
    pathscrub: windows
    filesystem:
      path: '/tmp'
      # We are looking for any video files that were extracted
      regexp: '.*\.(avi|mkv|mpg|mp4|m4v)$'
      recursive: yes
    regexp:
      reject:
        - \bsample\b: {from: title}
    content_size:
      min: 52

tasks:
  Sort_Unpacked_TV_Shows:
    # Where to get the unpacked (staged) files from
    filesystem:
      path: '{? dirs.stagshows ?}'
    # Where to get the metadata
    thetvdb_lookup: yes
    sequence:
      - configure_series:
          settings:
            tracking: no
          from:
            inputs:
            - trakt_list:
                account: '{? trakt.acct1 ?}'
                list: Following TV
                type: shows
                strip_dates: yes
            - trakt_list:
                account: '{? trakt.acct2 ?}'
                list: Following TV
                type: shows
                strip_dates: yes
            - trakt_list:
                account: '{? trakt.acct3 ?}'
                list: Following TV
                type: shows
                strip_dates: yes
            - trakt_list:
                account: '{? trakt.acct4 ?}'
                list: Following TV
                type: shows
                strip_dates: yes
    all_series:
      # Don't actually filter
      parse_only: yes
    # Don't attempt to move if we don't have valid metadata
    require_field: [tvdb_series_name, series_id]
    move:
      # Folder to move to
      to: >
        /mnt/media/shows/{{ tvdb_series_name|default(series_name)|replace('/', '_')|replace(':', '-')|replace(',', '.')|replace(' ', '.') }}/{% if series_id_type == 'ep' %}Season.{{ series_season|pad(2) }}/{% endif %}
      # Filename (inside of folder) to move to
      rename: >
        {{ tvdb_series_name|default(series_name)|replace('/', '_')|replace(':', '-')|replace(',', '')|replace(' ', '.') }}-{{ series_id }}{% if tvdb_ep_name|default(False) %}-{{ tvdb_ep_name|replace('/', '_')|replace(':', '-')|replace(',', '')|replace(' ', '.') }}{% endif %}-{{ quality }}.{{ location[-3:] }}
      clean_source: 7000
    if:
      - movedone:
          set:
            path: '{{ movedone }}'
    # tell plex to scan the folder we just moved into
    exec: ssh -i ~/.ssh/id_rsa plex@192.168.1.18 /usr/local/bin/bash /usr/local/bin/pms.script2 "/mnt/media/shows/{{ tvdb_series_name|default(series_name)|replace('/', '_')|replace(':', '-')|replace(',', '.')|replace(' ', '.') }}/{% if series_id_type == 'ep' %}Season.{{ series_season|pad(2) }}/{% endif %}"
    
  Sort_Unpacked_Season_Packs:
    # Where to get the unpacked (staged) files from
    filesystem:
      path: '{? dirs.stagpacks ?}'
    # Where to get the metadata
    thetvdb_lookup: yes
    sequence:
      - configure_series:
          settings:
            tracking: no
          from:
            inputs:
            - trakt_list:
                account: '{? trakt.acct1 ?}'
                list: Following TV
                type: shows
                strip_dates: yes
            - trakt_list:
                account: '{? trakt.acct2 ?}'
                list: Following TV
                type: shows
                strip_dates: yes
            - trakt_list:
                account: '{? trakt.acct3 ?}'
                list: Following TV
                type: shows
                strip_dates: yes
            - trakt_list:
                account: '{? trakt.acct4 ?}'
                list: Following TV
                type: shows
                strip_dates: yes
    all_series:
      # Don't actually filter
      parse_only: yes
    # Don't attempt to move if we don't have valid metadata
    require_field: [tvdb_series_name, series_id]
    move:
      # Folder to move to
      to: >
        /mnt/media/shows/{{ tvdb_series_name|default(series_name)|replace('/', '_')|replace(':', '-')|replace(',', '.')|replace(' ', '.') }}/{% if series_id_type == 'ep' %}Season.{{ series_season|pad(2) }}/{% endif %}
      # Filename (inside of folder) to move to
      rename: >
        {{ tvdb_series_name|default(series_name)|replace('/', '_')|replace(':', '-')|replace(',', '')|replace(' ', '.') }}-{{ series_id }}{% if tvdb_ep_name|default(False) %}-{{ tvdb_ep_name|replace('/', '_')|replace(':', '-')|replace(',', '')|replace(' ', '.') }}{% endif %}-{{ quality }}.{{ location[-3:] }}
      clean_source: 7000
    if:
      - movedone:
          set:
            path: '{{ movedone }}'
    # tell plex to scan the folder we just moved into
    exec: ssh -i ~/.ssh/id_rsa plex@192.168.1.18 /usr/local/bin/bash /usr/local/bin/pms.script2 "/mnt/media/shows/{{ tvdb_series_name|default(series_name)|replace('/', '_')|replace(':', '-')|replace(',', '.')|replace(' ', '.') }}/{% if series_id_type == 'ep' %}Season.{{ series_season|pad(2) }}/{% endif %}"
    
  Sort_Unpacked_TV_Premieres:
    # Where to get the unpacked (staged) files from
    filesystem:
      path: '{? dirs.stagprems ?}'
    # Where to get the metadata
    thetvdb_lookup: yes
    all_series:
      # Don't actually filter
      parse_only: yes
      # parse_only still filters propers
      propers: yes
    # Don't attempt to move if we don't have valid metadata
    require_field: [tvdb_series_name, series_id]
    move:
      # Folder to move to
      to: >
        /mnt/media/premieres/{{ tvdb_series_name|default(series_name)|replace('/', '_')|replace(':', '-')|replace(',', '')|replace(' ', '.') }}/{% if series_id_type == 'ep' %}Season.{{ series_season|pad(2) }}/{% endif %}
      # Filename (inside of folder) to move to
      rename: >
        {{ tvdb_series_name|default(series_name)|replace('/', '_')|replace(':', ' -')|replace(',', '')|replace(' ', '.') }}-{{ series_id }}{% if tvdb_ep_name|default(False) %}-{{ tvdb_ep_name|replace('/', '_')|replace(':', '-')|replace(',', '')|replace(' ', '.') }}{% endif %}-{{ quality }}.{{ location[-3:] }}
      clean_source: 7000
    # tell plex to scan the folder we just moved into
    exec: ssh -i ~/.ssh/id_rsa plex@192.168.1.18 /usr/local/bin/bash /usr/local/bin/pms.script2 "/mnt/media/premieres/{{ tvdb_series_name|default(series_name)|replace('/', '_')|replace(':', '-')|replace(',', '')|replace(' ', '.') }}/{% if series_id_type == 'ep' %}Season.{{ series_season|pad(2) }}/{% endif %}"
    
  # Same as above
  Sort_Unpacked_Movies:
    filesystem:
      path: '{? dirs.stagmovies ?}'
    imdb_lookup: yes
    tmdb_lookup: yes
    #rottentomatoes_lookup: yes
    proper_movies: yes
    require_field: [movie_name, movie_year]
    move:
      to: >
        /mnt/media/movies/{{ movie_name|replace('/', '_')|replace(':', '-')|replace(',', '')|replace(' ', '.') }}.{{ movie_year }}/
      rename: >
        {{ movie_name|replace('/', '_')|replace(':', ' -')|replace(',', '')|replace(' ', '.') }}-{{ movie_year }}-{{ quality }}.{{ location[-3:] }}
      clean_source: 7000
    # tell plex to scan the folder we just moved into
    exec: ssh -i ~/.ssh/id_rsa plex@192.168.1.18 /usr/local/bin/bash /usr/local/bin/pms.script2 "/mnt/media/movies/{{ movie_name|replace('/', '_')|replace(':', '-')|replace(',', '')|replace(' ', '.') }}.{{ movie_year }}/"
    
    
