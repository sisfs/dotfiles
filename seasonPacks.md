# What i did to get started with the season pack build was as follows.

1. `git checkout seasonPacks` to load the seasonPacks branch
2. execute `flexget -c qvazzler.yml trakt auth uname` to authorize THIS config to have access to trakt.tv
3. execute the following to create all the necessary files.
    ```shell
    flexget -c qvazzler.yml -l flexget-qvazzler.log -L debug execute --task get_series_packs --discover-now
    ```
4. on my machine this run didn't actually produce any results. if yours doesn't either proceed to the next step.
5. `mv db-qvazzler.sqlite db-qvazzler.sqlite.bak`
6. `cp db-config.sqlite db-qvazzler.sqlite`
7. i then re-ran the command from step 3.

This time the config identified a number of season packs from TL but timeframe as invoked because they didn't meet the target criteria... After the 
timeframe expired I ran the command again and 2 season packs were passsed to deluge.

### Let me know what issues arrise.


